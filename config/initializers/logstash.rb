# frozen_string_literal: true

Rails.application.configure do
  LogStashLogger.configure do |config|
    config.customize_event do |event|
      event['environment'] = Rails.env
      event['service'] = 'inventory-backend'
    end
  end
end
