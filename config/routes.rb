# frozen_string_literal: true

Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'reports_quantity_limit' => 'reports#quantity_limit'
  get 'vendor_report' => 'reports#vendor_report'
  get 'contractor_report' => 'reports#contractor_report'
  resources :categories
  resources :sites
  resources :products
  resources :categories do
    member do
      get :products
    end
  end
  resources :vendors
  resources :bills
  resources :shipments
  resources :contractors
  resources :bill_items
  resources :dispatched_items
  resources :dispatches
  resources :consumptions
  resources :consumed_items
  resources :repatriations
  resources :repatriated_items
  resources :reports
end
