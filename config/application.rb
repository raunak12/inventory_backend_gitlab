# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_mailbox/engine'
require 'action_text/engine'
require 'action_view/railtie'
require 'action_cable/engine'
require 'sprockets/railtie'
require 'rails/test_unit/railtie'
require 'logstash-logger'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
module InventoryBackend
  class Application < Rails::Application
    config.to_prepare do
      DeviseTokenAuth::SessionsController.skip_before_action :authenticate_user!, raise: false
      DeviseTokenAuth::PasswordsController.skip_before_action :authenticate_user!, raise: false
      DeviseTokenAuth::RegistrationsController.skip_before_action :authenticate_user!, raise: false
      DeviseTokenAuth::TokenValidationsController.skip_before_action :authenticate_user!, raise: false
    end
    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller

    config.api_only = true
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: %i[get post options delete put patch], expose: %w[
          access-token
          expiry
          client
          uid
          site_id
        ]
      end
    end

    config.middleware.use ActionDispatch::Flash
    config.session_store :cookie_store
    config.middleware.use ActionDispatch::Cookies # Required for all session management
    config.middleware.use ActionDispatch::Session::CookieStore, config.session_options
    config.middleware.use ::Rack::MethodOverride
    # Initialize configuration defaults for originally generated Rails version.
    config.autoload_paths << Rails.root.join('lib')
    config.eager_load_paths << Rails.root.join('lib')

    # To make sure that Subdomain class is autoloaded when the applicationn starts
    config.autoload_paths += %W[#{config.root}/lib]
  end
end
