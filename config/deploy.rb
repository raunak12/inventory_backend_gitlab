# frozen_string_literal: true

set :repo_url,        'git@bitbucket.org:codysseynepal/inventory-backend.git'
set :application,     'inventory_backend'
set :puma_threads,    [4, 16]
set :puma_workers,    0
set :user,            :root
# Don't change these unless you know what you're doing
set :pty,             false
set :use_sudo,        false
set :stages,          %i[production staging]
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     forward_agent: true, user: fetch(:user), keys: %w[~/.ssh/id_rsa.pub]
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true # Change to false when not using ActiveRecord
set :rbenv_ruby, '2.6.3'
# Server name for nginx, space separated values
# No default value

# Socket file that nginx will use as upstream to serve the application
# Note: Socket upstream has priority over host:port upstreams
# no default value
set :app_server_socket, "#{shared_path}/sockets/puma-#{fetch :application}.sock"

# The host that nginx will use as upstream to server the application
# default value: 127.0.0.1
set :app_server_host, '127.0.0.1'

# The port the application server is running on
# no default value
set :app_server_port, 8080

## Defaults:
# set :scm,           :git
set :branch, 'master'
# set :format,        :pretty
# set :log_level,     :debug
# set :keep_releases, 5
set :sentry_api_token, ENV['API_TOKEN']
set :sentry_organization, 'codyssey-web-nepal' # fetch(:application) by default
set :sentry_project, 'inventory-backend' # fetch(:application) by default
set :sentry_repo, 'codysseynepal/inventory-backend' # computed from repo_url by default
## Linked Files & Directories (Default None):
set :linked_files, %w[config/database.yml config/credentials.yml.enc config/master.key]
set :linked_dirs, %w[log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system]
set :linked_dirs, %w[public/uploads]
set :sidekiq_monit_use_sudo, false

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  desc 'Make sure local git is in sync with remote.'
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/#{fetch(:branch)}`
        puts "WARNING: HEAD is not the same as origin/#{fetch(:branch)}"
        puts 'Run `git push` to sync changes.'
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  before :starting, :check_revision
  # after :finishing, :compile_assets
  after  :finishing, :cleanup
  before 'deploy:starting', 'sentry:validate_config'
  after 'deploy:published', 'sentry:notice_deployment'
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma
