# frozen_string_literal: true

site_list = [
  ['Upper Tamakoshi, Dolakha', 'Narayan Sharma', '98493883'],
  ['Bungi Station, Kushma', 'Sita Upreti', '98112888'],
  ['Subba Construction, Balaju', 'Subodh Jha', '984444412'],
  ['Hetauda Construction, Balaju', 'Subodh Jha', '9844476'],
  ['jhapa Construction, Balaju', 'Subodh Jha', '984447609'],
  ['Budhanilkantha', "Baluwatar", "yogesh Bhattarai", "99888569549"]
]
puts '.............Creating Sites.................'
site_list.each do |name, contact_person, contact_num|
  Site.create(
    name: name,
    contact_person: contact_person,
    phone_no: contact_num
  )
end

site_id =
  Site.all.map(&:id)

puts '...............created Sites....................'

users = [
  ['roadshow@gmail.com', 'password', 'admin', site_id[0]],
  ['user@gmail.com', 'password', 'user', site_id[0]]
]
puts '..................creating users..............'

users.each do | email, password, role, site_id|
  User.create(
    email: email,
    password: password,
    role: role,
    site_id: site_id
  )
end
user_id =
  User.all.map(&:id)

puts '........created users ..............'

category_list = [
  ['Tile', 'Box', false, [], 123],
  ['Adhesive', 'Pcs', false, [], 300],
  ['Cement', 'Bags', true, [{ 'field' => 'kilogram', 'type' => 'float', 'unit' => 'KG' }], 1000],
  ['Grout', 'pkts', true, [{ 'field' => 'kilogram', 'type' => 'float', 'unit' => 'KG' }], 300],
  ['Wire', 'Coils', false, [], 500],
  ['Plumbing', 'Pcs', false, [], 500],
  ['Khaksi', 'Pcs', false, [], 1000],
  ['Fuel', 'Liter', false, [], 3000],
  ['Rod Dandi', 'Bundle', true, [{ 'field' => 'kilogram', 'type' => 'float', 'unit' => 'KG' }], 324],
  ['Housing Ingredients', 'Cft', false, [], 345],
  ['Wall Putty', 'Bags', false, [], 300],
  ['Paint', 'Buckets', false, [], 500],
  ['Ply', 'Pcs', false, [], 500],
  ['Wood', 'Qft', true, [
    { 'field' => 'length', 'type' => 'float', 'unit' => 'CM' },
    { 'field' => 'breadth', 'type' => 'float', 'unit' => 'CM' },
    { 'field' => 'height', 'type' => 'float', 'unit' => 'CM' }
  ], 345]
]

puts '......creating categories..........'
category_list.each do |name, unit, additional_field_required, additional_field, quantity_limit|
  Category.create(
    name: name,
    unit: unit,
    additional_field_required: additional_field_required,
    additional_fields: additional_field,
    quantity_limit: quantity_limit
  )
end

category_id =
  Category.all.map(&:id)

puts '........created categories ..............'



vendor_list = [
  ['Jagadamba', 'Subidhanagar', 555_512],
  ['Brij', 'Rampur', 334_567],
  ['Jay Bageshwori Trading Concern', 'Bhaktapur', 400_122],
  ['Berger Jenson & Nicholson (Nepal) Pvt. Ltd', 'Hetauda', 9_783_837],
  ['Shree Shrestha Enterprises Pvt. Ltd.', 'Kathmandu', 98_303_748],
  ['Homeland Colors & Sanitaries Pvt. Ltd.', 'jhapa', 983_737_383],
  ['Three Dimensional Engineering Consultant & Supplier Pvt. Ltd.', 'kathmandu', 938_394_948],
  ['Sunkosi Stone Indostries Pvt. Ltd.', 'Sunkoshi', 383_737_870],
  ['Himal Refrigeration and Electrical Industries Pvt. Ltd.', 'Kathmandu', 989_373_738],
  ['Ghorahi Cement Industry PVT.LTD', 'Ghorahi', 98_938_378],
  ['Brij Cement Industries Pvt. Ltd ', 'Hetauda', 3_373_838],
  ['Samart Cement Company Pvt.Ltd', 'KTM', 8_336_363_637],
  ['Kalimati Trader Center', 'KTM', 983_637_373]
]

puts '.............creating Vendors................'
vendor_list.each do |name, address, contact_num|
  Vendor.create(
    name: name,
    address: address,
    phone_no: contact_num
  )
end

vendor_id =
  Vendor.all.map(&:id)

puts 'vendor..................created Vendors................vendor.'

puts '.............creating Products........................'
product_list = [
  ['Wooden Sishow', category_id[13]],
  ['Italino Tile', category_id[0]],
  ['Super Glue', category_id[1]],
  ['Jagadamba Cement', category_id[2]],
  ['Brij Cement', category_id[2]],
  ['Birla Cement', category_id[2]],
  ['Bond tite', category_id[1]],
  ['Parking tile', category_id[0]],
  ['PVC Solvent Cement', category_id[2]],
  ['Bathroom Wall Tile', category_id[0]],
  ['Redbar', category_id[8]],
  ['Sand', category_id[9]],
  ['Pubble', category_id[9]],
  ['Ivory Grout', category_id[3]],
  ['Petrol', category_id[7]],
  ['Diesel', category_id[7]],
  ['8 X 4 Ply', category_id[12]]

]

product_list.each do |name, category_id|
  Product.create(
    name: name,
    category_id: category_id
  )
end

product_id =
  Product.all.map(&:id)

puts '.....................created Products.....................'

puts '.............creating Contractors........................'
contractor_list = [
  ['Binod Gupta', 'Sunsari', '9849999982'],
  ['Raji Khan', 'Kamaladi', '9801111125'],
  ['Ranjit Baitha', 'Ranibari', '9902233445']
]
contractor_list.each do |name, address, phone_no|
  Contractor.create(
    name: name,
    address: address,
    phone_no: phone_no
  )
end

contractor_id =
  Contractor.all.map(&:id)

puts '.....................created Contractors.....................'

bill_list = [
  [123_457, 10.days.before.to_date, 67_995, [1265, 85_596], vendor_id[2], site_id[0]],
  [2257, 5.days.before.to_date, 84_959, [55, 85_596], vendor_id[4], site_id[1]],
  [703_457, 2.days.before.to_date, 95, [20, 596], vendor_id[9], site_id[2]],
  [5506, 12.days.before.to_date, 993, [99_050, 59_611], vendor_id[8], site_id[2]],
  [123_457, 40.days.before.to_date, 995, [215, 896], vendor_id[7], site_id[1]],
  [157, 4.days.before.to_date, 5995, [2848, 896], vendor_id[5], site_id[2]],
  [17, 2.days.before.to_date, 59_005, [848, 996], vendor_id[5], site_id[0]],
  [917, 8.days.before.to_date, 9005, [84_448, 4996], vendor_id[5], site_id[2]],
  [918, 3.days.before.to_date, 7875, [84_448, 4996], vendor_id[2], site_id[5]],
  [919, 8.days.before.to_date, 5990, [84_448, 4996], vendor_id[3], site_id[5]],
  [920, 6.days.before.to_date, 8090, [84_448, 4996], vendor_id[12], site_id[0]]
]

puts '..................creating Bills......................'

bill_list.each do |bill_no, date, vehicle_no, chalan_no, vendor_id, site_id|
  Bill.create!(
    bill_no: bill_no,
    date: date,
    vehicle_no: vehicle_no,
    chalan_no: chalan_no,
    vendor_id: vendor_id,
    site_id: site_id
  )
end
bill_id = Bill.all.map(&:id)

puts '..................created Bills......................'

puts '................creating  shipments.......................'
shipment_list = [

  #purchase_items

  [4995, 2.days.before.to_date, 'Cft',
   { 'length' => 2, 'breadth' => 3, 'height' => 4 }, 'strong wood', product_id[0], 'Vendor', vendor_id[0], 'Site', site_id[1]],
  [88_485, 10.days.before.to_date, 'Box', {}, 'white tile', product_id[1], 'Vendor', vendor_id[0], 'Site', site_id[1]],
  [885, 5.days.before.to_date, 'Pcs', {}, 'strong wood', product_id[2], 'Vendor', vendor_id[2], 'Site', site_id[4]],
  [4605, 25.days.before.to_date, 'Bags', { 'kilogram' => '68', 'type' => 'float', 'unit' => 'KG' }, 'white Cement', product_id[3], 'Vendor', vendor_id[4], 'Site', site_id[2]],
  [6905, 7.days.before.to_date, 'Bags', { 'kilogram' => '56', 'type' => 'float', 'unit' => 'KG' }, 'good quality', product_id[4], 'Vendor', vendor_id[5], 'Site', site_id[0]],
  [805, 1.days.before.to_date, 'Pcs', {}, 'Chinese Adhesive', product_id [6], 'Vendor', vendor_id[8], 'Site', site_id[2]],
  [1905, 1.days.before.to_date, 'Box', {}, 'square tile', product_id[7], 'Vendor', vendor_id[7], 'Site', site_id[1]],
  [77_905, 3.days.before.to_date, 'Cft', {}, 'refined', product_id[9], 'Vendor', vendor_id[4], 'Site', site_id[1]],
  [55_905, 9.days.before.to_date, 'Bags', {}, 'refined', product_id[9], 'Vendor', vendor_id[8], 'Site', site_id[0]],
  [66_905, 5.days.before.to_date, 'Bundle', {}, 'refined', product_id[9], 'Vendor', vendor_id[8], 'Site', site_id[1]],
  [66_905, 5.days.before.to_date, 'Liter', {}, 'refined', product_id[14], 'Vendor', vendor_id[10], 'Site', site_id[5]],
  [66_905, 3.days.before.to_date, 'pcs', {}, 'Euro 4', product_id[14],    'Vendor', vendor_id[2], 'Site', site_id[5]],

  
  #consumed_items 11

  [445, 2.days.before.to_date, 'Liter', {}, 'Euro 4', product_id[14], 'Site', site_id[2], 'Contractor', contractor_id[0]],
  [60, 9.days.before.to_date, 'Bags', { 'kilogram' => '56', 'type' => 'float', 'unit' => 'KG' }, 'Refined', product_id[9], 'Site', site_id[1], 'Contractor', contractor_id[1]],
  [25, 6.days.before.to_date, 'pcs', {}, 'Refined', product_id[2], 'Site', site_id[4], 'Contractor', contractor_id[1]],
  [65, 3.days.before.to_date, 'bags', {}, 'Refined', product_id[4], 'Site', site_id[0], 'Contractor', contractor_id[2]],
  [80, 8.days.before.to_date, 'Qft', {}, 'Refined', product_id[0], 'Site', site_id[1], 'Contractor', contractor_id[0]],
  [120, 12.days.before.to_date, 'cft', {}, 'Refined', product_id[7], 'Site', site_id[1], 'Contractor', contractor_id[1]],
  [85, 7.days.before.to_date, 'box', {}, 'Refined', product_id[7], 'Site', site_id[1], 'Contractor', contractor_id[2]],

  #repartiated_items 18

  [5, 7.days.before.to_date, 'Liter', {}, 'Refined', product_id[14], 'Contractor', contractor_id[0], 'Site', site_id[0]],
  [3, 3.days.before.to_date, 'Liter', {}, 'Euro 4', product_id[14],    'Contractor', contractor_id[0], 'Site', site_id[0]],
  [7, 20.days.before.to_date, 'Qft', {}, 'good quality', product_id[0], 'Contractor', contractor_id[0], 'Site', site_id[0]],
  [9, 25.days.before.to_date, 'Liter', {}, 'Euro 4', product_id[14], 'Contractor', contractor_id[0], 'Site', site_id[1]],
  [5, 2.days.before.to_date, 'Cft',
  { 'length' => 2, 'breadth' => 3, 'height' => 4 }, 'strong wood', product_id[0], 'Contractor', contractor_id[0], 'Site', site_id[1]],

  #dispatch items 23

  [13, 3.days.before.to_date, 'Qft', {}, 'Strong', product_id[0],    'Site', site_id[1], 'Site', site_id[5]],
  [12, 2.days.before.to_date, 'Bags', {}, 'Solvent', product_id[5],    'Site', site_id[5], 'Site', site_id[2]],
  [13, 1.days.before.to_date, 'Box', {}, 'square', product_id[9],    'Site', site_id[1], 'Site', site_id[5]]
]
shipment_list.each do |quantity, date, unit, additional_units, remarks, product_id, source_type, source_id, destination_type, destination_id|
  Shipment.create(
    quantity: quantity,
    date: date,
    unit: unit,
    additional_units: additional_units,
    remarks: remarks,
    product_id: product_id,
    source_type: source_type,
    source_id: source_id,
    destination_type: destination_type,
    destination_id: destination_id
  )
end
shipment_id =
  Shipment.all.map(&:id)
puts '................created Shipments.......................'

puts '......................Consumptions Creating..................'

consumption_list = [
  [5.days.before.to_date, '13 Pragati Marg '],
  [1.days.before.to_date, '13 Hallan Chowk '],
  [4.days.before.to_date, '14 Rapti Sadak '],
  [4.days.before.to_date, '3 Ghattekulo Marg '],
  [2.days.before.to_date, '34 Anamnagar Marg '],
  [15.days.before.to_date, '903 Putalisadak Marg '],
  [51.days.before.to_date, '103 Bhakta Marg '],
  [8.days.before.to_date, '345 Harihar Marg '],
  [10.days.before.to_date, '3 Narayan Marg '],
  [25.days.before.to_date, '55413 Pulchowk Marg ']
]
consumption_list.each do |date, building_no|
  Consumption.create(
    date: date,
    building_no: building_no
  )
end

consumption_id = Consumption.all.map(&:id)
puts '......................Consumptions Created..................'

puts '.............Consumed_items Creating..............'
consumption_items_list = [
  [consumption_id[1], shipment_id[17]],
  [consumption_id[5], shipment_id[12]],
  [consumption_id[7], shipment_id[13]],
  [consumption_id[1], shipment_id[14]],
  [consumption_id[9], shipment_id[15]],
  [consumption_id[6], shipment_id[16]]
]
consumption_items_list.each do |consumption_id, shipment_id|
  ConsumedItem.create(
    consumption_id: consumption_id,
    shipment_id: shipment_id
  )
end

consumption_items_id = ConsumedItem.all.map(&:id)
puts '................created consumed Items...............'

puts '.....................Creating Dispatch.................'
dispatch_list = [
  ['0101', '010', 'ba 47 pa 1425', 7.days.before.to_date],
  ['0102', '011', 'ba 14 pa 1225', 6.days.before.to_date],
  ['0103', '012', 'ba 49 pa 1435', 8.days.before.to_date],
  ['0104', '013', 'ba 49 pa 1425', 5.days.before.to_date],
  ['0105', '014', 'ba 50 pa 1485', 2.days.before.to_date],
  ['0106', '013', 'ba 60 pa 1425', 4.days.before.to_date],
  ['0107', '014', 'ba 52 pa 1295', 3.days.before.to_date],
  ['76545', '015', 'ba 52 pa 125', 2.days.before.to_date],
  ['107', '016', 'ba 52 pa 95', 1.days.before.to_date],
  ['5607', '017', 'ba 52 pa 295', 5.days.before.to_date],


]
dispatch_list.each do |chalan_no, bill_no, vehicle_no, date|
  Dispatch.create(
    chalan_no: chalan_no,
    bill_no: bill_no,
    vehicle_no: vehicle_no,
    date: date
  )
end
dispatch_id = Dispatch.all.map(&:id)
puts '..........created Dispatch..................'

puts '.....................Creating Dispatched Items.................'
dispatched_item_list = [
  [shipment_id[26], dispatch_id[0]],
  [shipment_id[24], dispatch_id[0]],
  [shipment_id[25], dispatch_id[0]],

]
dispatched_item_list.each do |shipment_id, dispatch_id|
  DispatchedItem.create(
    dispatch_id: dispatch_id,
    shipment_id: shipment_id
  )
end
puts '..........created Dispatched Items..................'

puts '................creating Bill Items............'
bill_items_list = [
  [1000.34, 40, 455_706, shipment_id[0], bill_id[1]],
  [200, 40, 246_006, shipment_id[1], bill_id[4]],
  [5000.34, 40, 400_006, shipment_id[2], bill_id[6]],
  [7000.34, 4, 50_006, shipment_id[3], bill_id[1]],
  [9000.34, 80, 6_400_006, shipment_id[4], bill_id[4]],
  [2000.34, 90, 800_006, shipment_id[5], bill_id[3]],
  [8000.34, 70, 48_906, shipment_id[6], bill_id[2]],
  [1000.34, 670, 10_006, shipment_id[7], bill_id[0]],
  [9000.34, 540, 403_007, shipment_id[8], bill_id[6]],
  [55_000.34, 20, 889_006, shipment_id[9], bill_id[2]],
  [55_000.34, 20, 889_006, shipment_id[10], bill_id[2]],
  [6648, 25, 225_456, shipment_id[11], bill_id[6]],
  [478, 5, 5456, shipment_id[12], bill_id[9]],
  [8938, 28, 99456, shipment_id[25], bill_id[5]],

]

bill_items_list.each do |rate, excise, amount, shipment_id, bill_id|
  BillItem.create(
    rate: rate,
    excise: excise,
    amount: amount,
    shipment_id: shipment_id,
    bill_id: bill_id
  )
end
puts '....................Created bill items.................'

puts '.......................Creating Repartation...............'
repatriation_list = [
  [7.days.before.to_date, "ba 2 kha 4995", [34, 67], "product exceed for this site"],
  [2.days.before.to_date, "Na 1 jha 4995", [67], "Delay to run project"],
  [5.days.before.to_date, "Dha 9 kha 4995", [], "cancled the project"],
  [Date.today.to_date, "Me 1 kha 4995", [ 567], "Not needed this product"],
  [1.days.before.to_date, "Ra 2 kha 4995", [76], "product exceed for this site"],
  [20.days.before.to_date, "ba 2 kha 4995", [934, 167], "Delay to construct site"] 
]

repatriation_list.each do |date, building_no, vehicle_no, chalan_no, reason|
  Repatriation.create(
    date: date,
    vehicle_no: vehicle_no,
    chalan_no: chalan_no,
    reason: reason
  )
end
repatriation_id =
  Repatriation.all.map(&:id)

puts ".........created Repatriations................"

puts ".........created Repatriation items.............."
repatriation_item_list = [
  [repatriation_id[0], shipment_id[19]],
  [repatriation_id[1], shipment_id[20]],
  [repatriation_id[3], shipment_id[21]],
  [repatriation_id[2], shipment_id[22]],
  [repatriation_id[4], shipment_id[23]]
]
repatriation_item_list.each do |repatriation_id, shipment_id|
  RepatriatedItem.create(
    repatriation_id: repatriation_id,
    shipment_id: shipment_id
  )
end

puts ".............Created Repatriation Items................"
