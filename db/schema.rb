# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_03_185043) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bill_items", force: :cascade do |t|
    t.float "rate"
    t.float "excise"
    t.float "amount"
    t.float "discount"
    t.bigint "shipment_id"
    t.bigint "bill_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bill_id"], name: "index_bill_items_on_bill_id"
    t.index ["shipment_id"], name: "index_bill_items_on_shipment_id"
  end

  create_table "bills", force: :cascade do |t|
    t.bigint "bill_no"
    t.date "date"
    t.string "vehicle_no"
    t.bigint "chalan_no", default: [], array: true
    t.bigint "vendor_id"
    t.bigint "site_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bill_no", "vendor_id"], name: "index_bills_on_bill_no_and_vendor_id", unique: true
    t.index ["site_id"], name: "index_bills_on_site_id"
    t.index ["vendor_id"], name: "index_bills_on_vendor_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "unit"
    t.boolean "additional_field_required"
    t.jsonb "additional_fields", default: []
    t.float "quantity_limit"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "consumed_items", force: :cascade do |t|
    t.bigint "consumption_id"
    t.bigint "shipment_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["consumption_id"], name: "index_consumed_items_on_consumption_id"
    t.index ["shipment_id"], name: "index_consumed_items_on_shipment_id"
  end

  create_table "consumptions", force: :cascade do |t|
    t.date "date"
    t.string "building_no"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "contractors", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.bigint "phone_no"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "dispatched_items", force: :cascade do |t|
    t.bigint "dispatch_id"
    t.bigint "shipment_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["dispatch_id"], name: "index_dispatched_items_on_dispatch_id"
    t.index ["shipment_id"], name: "index_dispatched_items_on_shipment_id"
  end

  create_table "dispatches", force: :cascade do |t|
    t.bigint "chalan_no", default: [], array: true
    t.bigint "bill_no"
    t.string "vehicle_no"
    t.date "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.bigint "category_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_products_on_category_id"
  end

  create_table "repatriated_items", force: :cascade do |t|
    t.bigint "repatriation_id"
    t.bigint "shipment_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["repatriation_id"], name: "index_repatriated_items_on_repatriation_id"
    t.index ["shipment_id"], name: "index_repatriated_items_on_shipment_id"
  end

  create_table "repatriations", force: :cascade do |t|
    t.date "date"
    t.string "vehicle_no"
    t.bigint "chalan_no", default: [], array: true
    t.string "reason"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "shipments", force: :cascade do |t|
    t.float "quantity"
    t.date "date"
    t.string "unit"
    t.string "brand"
    t.string "description"
    t.jsonb "additional_units", default: {}
    t.string "remarks"
    t.bigint "product_id"
    t.string "source_type"
    t.bigint "source_id"
    t.string "destination_type"
    t.bigint "destination_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["date"], name: "index_shipments_on_date"
    t.index ["destination_type", "destination_id"], name: "index_shipments_on_destination_type_and_destination_id"
    t.index ["product_id"], name: "index_shipments_on_product_id"
    t.index ["source_type", "source_id"], name: "index_shipments_on_source_type_and_source_id"
  end

  create_table "sites", force: :cascade do |t|
    t.string "name"
    t.string "contact_person"
    t.bigint "phone_no"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "role"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "email"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.bigint "site_id"
    t.json "tokens"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["site_id"], name: "index_users_on_site_id"
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "vendors", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.bigint "phone_no"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "bill_items", "shipments"
  add_foreign_key "bills", "sites"
  add_foreign_key "bills", "vendors"
  add_foreign_key "products", "categories"
  add_foreign_key "shipments", "products"
  add_foreign_key "users", "sites"
end
