# frozen_string_literal: true

class CreateShipments < ActiveRecord::Migration[6.0]
  def change
    create_table :shipments do |t|
      t.float :quantity
      t.date :date, index: true
      t.string :unit
      t.string :brand
      t.string :description
      t.jsonb :additional_units, default: {}
      t.string :remarks
      t.references :product, foreign_key: true, index: true
      t.references :source, polymorphic: true, index: true
      t.references :destination, polymorphic: true, index: true

      t.timestamps
    end
  end
end
