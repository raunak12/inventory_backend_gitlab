# frozen_string_literal: true

class CreateDispatches < ActiveRecord::Migration[6.0]
  def change
    create_table :dispatches do |t|
      t.bigint :chalan_no, array: true, default: []
      t.bigint :bill_no
      t.string :vehicle_no
      t.date :date

      t.timestamps
    end
  end
end
