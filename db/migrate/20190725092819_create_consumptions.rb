# frozen_string_literal: true

class CreateConsumptions < ActiveRecord::Migration[6.0]
  def change
    create_table :consumptions do |t|
      t.date :date
      t.string :building_no

      t.timestamps
    end
  end
end
