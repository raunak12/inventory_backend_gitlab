# frozen_string_literal: true

class CreateRepatriations < ActiveRecord::Migration[6.0]
  def change
    create_table :repatriations do |t|
      t.date :date
      t.string :vehicle_no
      t.bigint :chalan_no, array: true, default: []
      t.string :reason

      t.timestamps
    end
  end
end
