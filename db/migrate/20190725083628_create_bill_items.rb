# frozen_string_literal: true

class CreateBillItems < ActiveRecord::Migration[6.0]
  def change
    create_table :bill_items do |t|
      t.float :rate
      t.float :excise
      t.float :amount
      t.float :discount
      t.references :shipment, foreign_key: true
      t.references :bill

      t.timestamps
    end
  end
end
