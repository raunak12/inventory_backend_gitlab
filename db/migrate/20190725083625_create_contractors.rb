# frozen_string_literal: true

class CreateContractors < ActiveRecord::Migration[6.0]
  def change
    create_table :contractors do |t|
      t.string :name
      t.string :address
      t.bigint :phone_no

      t.timestamps
    end
  end
end
