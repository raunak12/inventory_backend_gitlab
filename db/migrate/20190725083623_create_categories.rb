# frozen_string_literal: true

class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.string :unit
      t.boolean :additional_field_required
      t.jsonb :additional_fields, default: []
      t.float :quantity_limit

      t.timestamps
    end
  end
end
