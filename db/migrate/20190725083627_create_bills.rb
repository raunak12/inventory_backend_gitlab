# frozen_string_literal: true

class CreateBills < ActiveRecord::Migration[6.0]
  def change
    create_table :bills do |t|
      t.bigint :bill_no
      t.date :date
      t.string :vehicle_no
      t.bigint :chalan_no, array: true, default: []
      t.references :vendor, foreign_key: true
      t.references :site, foreign_key: true

      t.timestamps
    end
    add_index :bills, %i[bill_no vendor_id], unique: true
  end
end
