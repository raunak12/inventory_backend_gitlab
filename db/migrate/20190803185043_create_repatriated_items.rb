# frozen_string_literal: true

class CreateRepatriatedItems < ActiveRecord::Migration[6.0]
  def change
    create_table :repatriated_items do |t|
      t.references :repatriation
      t.references :shipment

      t.timestamps
    end
  end
end
