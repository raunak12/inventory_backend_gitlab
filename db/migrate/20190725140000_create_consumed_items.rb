# frozen_string_literal: true

class CreateConsumedItems < ActiveRecord::Migration[6.0]
  def change
    create_table :consumed_items do |t|
      t.references :consumption
      t.references :shipment

      t.timestamps
    end
  end
end
