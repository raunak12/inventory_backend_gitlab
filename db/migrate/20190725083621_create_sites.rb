# frozen_string_literal: true

class CreateSites < ActiveRecord::Migration[6.0]
  def change
    create_table :sites do |t|
      t.string :name
      t.string :contact_person
      t.bigint :phone_no

      t.timestamps
    end
  end
end
