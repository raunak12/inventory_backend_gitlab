# frozen_string_literal: true

class CreateVendors < ActiveRecord::Migration[6.0]
  def change
    create_table :vendors do |t|
      t.string :name
      t.string :address
      t.bigint :phone_no

      t.timestamps
    end
  end
end
