# frozen_string_literal: true

class CreateDispatchedItems < ActiveRecord::Migration[6.0]
  def change
    create_table :dispatched_items do |t|
      t.references :dispatch
      t.references :shipment

      t.timestamps
    end
  end
end
