# frozen_string_literal: true

class RepatriationsController < ApplicationController
  before_action :set_repatriation, only: %i[update show destroy]
  respond_to :json

  def index
    repatriations = if admin?(current_user)
                      Repatriation.all
                    else
                      Consumption.joins(:shipment).where(shipments: { source_id: current_user.site_id })
                    end
    render json: repatriations
  end

  def show
    render json: @repatriation
  end

  def create
    repatriation = Repatriation.new(repatriation_params)
    if repatriation.save
      render json: repatriation, status: :created
    else
      render json: repatriation.errors, status: :unprocessable_entity
    end
  end

  def update
    if @repatriation.update(repatriation_params)
      render json: @repatriation
    else
      render json: @repatriation.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @repatriation.destroy
    head :no_content
  end

  private

  def set_repatriation
    @repatriation = Repatriation.find(params[:id])
  end

  def repatriation_params
    params.require(:repatriation)
          .permit(:date, :reason, repatriated_items_attributes:
                  [:id, shipment_attributes: %i[id quantity date unit
                                                brand remarks product_id description source_type source_id
                                                destination_type destination_id]], chalan_no: [])
          .tap do |whitelisted|
            repatriated_items = whitelisted.dig(:repatriated_items_attributes) || []
            whitelisted[:repatriated_items_attributes] =
              repatriated_items.each do |repatriated_item|
                repatriated_item[:shipment_attributes][:source_id] = params.dig(:source_id)
                repatriated_item[:shipment_attributes][:source_type] = params.dig(:source_type)
                repatriated_item[:shipment_attributes][:destination_id] = params.dig(:destination_id)
                repatriated_item[:shipment_attributes][:destination_type] = params.dig(:destination_type)
                repatriated_item[:shipment_attributes][:date] = params.dig(:date)
                repatriated_item
              end
          end
  end
end
