# frozen_string_literal: true

class ReportsController < ApplicationController
  def index
    report_data =
      Shipment.final_site_reports(report_options)
              .map(&method(:build_site_report_data))
    render json: report_data, status: :ok
  end

  def quantity_limit
    report_data =
      Shipment.final_site_reports(report_options)
              .joins(product: :category).where('categories.quantity_limit > shipments.closing')
              .map(&method(:build_site_report_data))

    render json: report_data, status: :ok
  end

  def vendor_report
    report_data =
      Shipment.final_vendor_reports(report_options)
              .map(&method(:build_vendor_params))
    render json: report_data, status: :ok
  end

  def contractor_report
    report_data =
      Shipment.final_contractor_reports(report_options)
              .map(&method(:build_contractor_params))
    render json: report_data, status: :ok
  end

  private

  def report_options
    {
      start_date: params[:start_date],
      end_date: params[:end_date],
      site_id: params[:site_id],
      product_id: params[:product_id],
      vendor_id: params[:vendor_id],
      contractor_id: params[:contractor_id]
    }
  end

  def build_site_report_data(shipment)
    {
      site_name: shipment.site_name,
      quantity_limit: shipment.product.category.quantity_limit,
      site_id: shipment.site_id,
      product_id: shipment.product_id,
      product_name: shipment.product.name,
      purchase: shipment.total_purchase,
      consumption: shipment.total_consumed,
      dispatch_in: shipment.total_dispatch_in,
      dispatch_out: shipment.total_dispatch_out,
      repatriation: shipment.total_repatriation,
      closing: shipment.closing,
      unit: shipment.product.category.unit
    }
  end

  def build_vendor_params(shipment)
    {
      quantity: shipment.total_quantity,
      unit: shipment.product.category.unit,
      product_name: shipment.product.name,
      vendor_name: shipment.vendor_name
    }
  end

  def build_contractor_params(shipment)
    {
      quantity: shipment.total_consumed,
      unit: shipment.product.category.unit,
      product_name: shipment.product.name,
      consumption: shipment.total_consumption,
      repatriation: shipment.total_repatriation,
      contractor_name: shipment.contractor_name
    }
  end
end
