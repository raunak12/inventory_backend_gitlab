# frozen_string_literal: true

class ProductsController < ApplicationController # :nodoc:
  before_action :set_product, only: %i[update show destroy]
  respond_to :json

  def index
    respond_with Product.all
  end

  def show
    render json: @product
  end

  def create
    product = Product.new(product_params)
    return unauthorized unless admin?(current_user)

    if product.save
      render json: product, status: :created
    else
      render json: product.errors, status: :unprocessable_entity
    end
  end

  def update
    return unauthorized unless admin?(current_user)

    if @product.update(product_params)
      render json: @product, status: :ok
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  def destroy
    return unauthorized unless admin?(current_user)

    @product.destroy
    head :no_content
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :category_id)
  end
end
