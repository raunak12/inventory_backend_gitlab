# frozen_string_literal: true

class CategoriesController < ApplicationController # :nodoc:
  before_action :set_category, only: %i[update show destroy products]

  def index
    categories = Category.all
    render json: categories
  end

  def show
    render json: @category
  end

  def create
    category = Category.new(category_params)

    return unauthorized unless admin?(current_user)

    if category.save
      render json: category, status: :created
    else
      render json: category.errors, status: :unprocessable_entity
    end
  end

  def update
    return unauthorized unless admin?(current_user)

    if @category.update(category_params)
      render json: @category, status: :ok
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  def destroy
    return unauthorized unless admin?(current_user)

    @category.destroy
    head :no_content
  end

  def products
    products =
      @category.products.map do |product|
        {
          id: product.id,
          name: product.name
        }
      end
    render json: products
  end

  private

  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name, :unit, :quantity_limit, :additional_field_required, additional_fields: %i[field type unit])
  end
end
