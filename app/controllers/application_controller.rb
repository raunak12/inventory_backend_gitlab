a
b
# frozen_string_literal: true

require 'application_responder'

class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  before_action :authenticate_user!

  def unauthorized
    render json: { 'error': 'Unauthorized' }, status: 403
  end

  def admin?(current_user)
    current_user.role == 'admin'
  end
end
