# frozen_string_literal: true

class ConsumedItemsController < ApplicationController
  def index
    consumed_items =
      if admin?(current_user)
        ConsumedItem.all
      else
        ConsumedItem.joins(:shipment).where(shipments: { source_id: current_user.site_id })
      end
    render json: consumed_items
  end

  def show
    consumed_items = ConsumedItem.find(params[:id])
    render json: consumed_items
  end
end
