# frozen_string_literal: true

class ShipmentsController < ApplicationController
  before_action :set_shipment, only: %i[update show destroy]
  respond_to :json

  def index
    shipments = Shipment.all
    render json: shipments
  end

  def show
    render json: @shipment
  end

  def create
    shipment = Shipment.new(shipment_params)
    if shipment.save
      render json: shipment, status: :created
    else
      render json: shipment.errors, status: :unprocessable_entity
    end
  end

  def update
    if @shipment.update(shipment_params)
      render json: @shipment, status: :ok
    else
      render json: @shipment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @shipment.destroy
    head :no_content
  end

  private

  def set_shipment
    @shipment = Shipment.find(params[:id])
  end

  def shipment_params
    params.require(:shipment).permit(:quantity, :date, :unit, :brand,
                                     :description, :additional_units,
                                     :remarks, :product_id,
                                     :source_id, :source_type,
                                     :destination_id, :destination_type)
  end
end
