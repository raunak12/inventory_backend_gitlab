# frozen_string_literal: true

class BillsController < ApplicationController # :nodoc:
  before_action :set_bill, only: %i[update show destroy]
  respond_to :json

  def index
    bills = Bill.all
    render json: bills
  end

  def show
    render json: @bill
  end

  def create
    bill = Bill.new(bill_params)
    if bill.save
      render json: bill, status: :created
    else
      render json: bill.errors, status: :unprocessable_entity
    end
  end

  def update
    if @bill.update(bill_params)
      render json: @bill, status: 200
    else
      render json: @bill.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @bill.destroy
    head :no_content
  end

  private

  def set_bill
    @bill = Bill.find(params[:id])
  end

  def bill_params
    params.require(:bill)
          .permit(:date, :bill_no, :vendor_id, :vehicle_no, :site_id, bill_items_attributes:
                  [:id, :rate, :excise, :amount, :discount, shipment_attributes:
                    %i[id quantity date unit brand remarks product_id description]], chalan_no: [])
          .tap do |whitelisted|
            whitelisted[:bill_items_attributes] = build_bill_items(whitelisted)
          end
  end

  def build_bill_items(whitelisted)
    bill_items = whitelisted.dig(:bill_items_attributes) || []
    bill_items.each_with_index.map do |bill_item, index|
      additional_fields = additional_fields_for_bill_item(bill_item)
      bill_item[:shipment_attributes][:additional_units] =
        additional_fields.each_with_object({}) do |additional_field, accumulator|
          accumulator.merge!(permit_additional_info(additional_field, index))
        end
      bill_item[:shipment_attributes].merge!(permit_source_destination(whitelisted))
      bill_item
    end
  end

  def additional_fields_for_bill_item(bill_item)
    category = Product.find_by(id: bill_item.dig(:shipment_attributes, :product_id))&.category
    category&.additional_field_required? && category&.additional_fields || []
  end

  def permit_additional_info(additional_field, index)
    field_name = additional_field['field']
    {
      field_name.to_sym => params.dig(:bill, :bill_items_attributes, index,
                                      :shipment_attributes, :additional_units,
                                      field_name)
    }
  end

  def permit_source_destination(whitelisted)
    {
      source_id: whitelisted.dig(:vendor_id),
      source_type: 'Vendor',
      destination_id: whitelisted.dig(:site_id),
      destination_type: 'Site',
      date: whitelisted.dig(:date)
    }
  end
end
