# frozen_string_literal: true

class DispatchedItemsController < ApplicationController
  def index
    dispatched_items = if admin?(current_user)
                         DispatchedItem.all
                       else
                         DispatchedItem.joins(:shipment).where('(shipments.source_id= ? OR shipments.destination_id=?)', current_user.site_id, current_user.site_id)

                       end
    render json: dispatched_items
  end

  def show
    dispatched_items = DispatchedItem.find(params[:id])
    render json: dispatched_items
  end
end
