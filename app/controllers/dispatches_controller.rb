# frozen_string_literal: true

class DispatchesController < ApplicationController # :nodoc:
  before_action :set_dispatch, only: %i[update show destroy]
  respond_to :json

  def index
    dispatches = Dispatch.all
    render json: dispatches
  end

  def show
    render json: @dispatch
  end

  def create
    dispatch = Dispatch.new(dispatch_params)
    if dispatch.save
      render json: dispatch, status: :created
    else
      render json: dispatch.errors, status: :unprocessable_entity
    end
  end

  def update
    if @dispatch.update(dispatch_params)
      render json: @dispatch
    else
      render json: @dispatch.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @dispatch.destroy
    head :no_content
  end

  private

  def set_dispatch
    @dispatch = Dispatch.find(params[:id])
  end

  def dispatch_params
    params.require(:dispatch)
          .permit(:date, :bill_no, :vehicle_no, dispatched_items_attributes:
                  [:id, shipment_attributes: %i[id quantity date unit
                                                brand remarks source_id destination_id product_id description]], chalan_no: [])
          .tap do |whitelisted|
            dispatched_items = whitelisted.dig(:dispatched_items_attributes) || []
            whitelisted[:dispatched_items_attributes] =
              dispatched_items.each do |dispatched_item|
                dispatched_item[:shipment_attributes][:source_type] = 'Site'
                dispatched_item[:shipment_attributes][:source_id] = params.dig(:source_id)
                dispatched_item[:shipment_attributes][:destination_id] = params.dig(:destination_id)
                dispatched_item[:shipment_attributes][:destination_type] = 'Site'
                dispatched_item[:shipment_attributes][:date] = params.dig(:date)
                dispatched_item
              end
          end
  end
end
