# frozen_string_literal: true

class SitesController < ApplicationController # :nodoc:
  before_action :set_site, only: %i[show update destroy]
  respond_to :json

  def index
    respond_with Site.all
  end

  def show
    render json: @site
  end

  def create
    return unauthorized unless admin?(current_user)

    respond_with Site.create(site_params)
  end

  def destroy
    return unauthorized unless admin?(current_user)

    respond_with Site.destroy(params[:id])
    head :no_content
  end

  def update
    site = Site.find(params['id'])
    return unauthorized unless admin?(current_user)

    site.update(site_params)
    respond_with Site, json: site
  end

  private

  def set_site
    @site = Site.find(params[:id])
  end

  def site_params
    params.require(:site).permit(:contact_person, :name, :phone_no)
  end
end
