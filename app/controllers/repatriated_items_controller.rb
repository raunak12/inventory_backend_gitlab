# frozen_string_literal: true

class RepatriatedItemsController < ApplicationController
  def index
    repatriated_items =
      if admin?(current_user)
        RepatriatedItem.all
      else
        RepatriatedItem.joins(:shipment).where(shipments: { destination_id: current_user.site_id })
      end
    render json: repatriated_items
  end

  def show
    repatriated_item = RepatriatedItem.find(params[:id])
    render json: repatriated_item
  end
end
