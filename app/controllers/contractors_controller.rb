# frozen_string_literal: true

class ContractorsController < ApplicationController # :nodoc:
  before_action :set_contractor, only: %i[show update destroy]
  respond_to :json

  def index
    respond_with Contractor.all
  end

  def show
    render json: @contractor
  end

  def create
    return unauthorized unless authorized(current_user)

    respond_with Contractor.create(contractor_params)
  end

  def destroy
    return unauthorized unless admin?(current_user)

    @contractor.destroy
    head :no_content
  end

  def update
    contractor = Contractor.find(params['id'])
    return unauthorized unless admin?(current_user)

    contractor.update(contractor_params)
    respond_with contractor, json: contractor
  end

  private

  def set_contractor
    @contractor = Contractor.find(params[:id])
  end

  def contractor_params
    params.require(:contractor).permit(:name, :address, :phone_no)
  end
end
