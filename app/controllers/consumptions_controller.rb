# frozen_string_literal: true

class ConsumptionsController < ApplicationController # :nodoc:
  before_action :set_consumption, only: %i[update show destroy]
  respond_to :json

  def index
    consumptions = Consumption.all
    render json: consumptions
  end

  def show
    render json: @consumption
  end

  def create
    consumption = Consumption.new(consumption_params)
    if consumption.save
      render json: consumption, status: :created
    else
      render json: consumption.errors, status: :unprocessable_entity
    end
  end

  def update
    if @consumption.update(consumption_params)
      render json: @consumption
    else
      render json: @consumption.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @consumption.destroy
    head :no_content
  end

  private

  def set_consumption
    @consumption = Consumption.find(params[:id])
  end

  def consumption_params
    params.require(:consumption)
          .permit(:date, :building_no, consumed_items_attributes:
                  [:id, shipment_attributes: %i[id quantity date unit
                                                brand remarks product_id description source_type source_id destination_type destination_id]], chalan_no: [])
          .tap do |whitelisted|
            consumed_items = whitelisted.dig(:consumed_items_attributes) || []
            whitelisted[:consumed_items_attributes] =
              consumed_items.each do |consumed_item|
                consumed_item[:shipment_attributes][:source_id] = params.dig(:source_id)
                consumed_item[:shipment_attributes][:source_type] = params.dig(:source_type)
                consumed_item[:shipment_attributes][:destination_id] = params.dig(:destination_id)
                consumed_item[:shipment_attributes][:destination_type] = params.dig(:destination_type)
                consumed_item[:shipment_attributes][:date] = params.dig(:date)
                consumed_item
              end
          end
  end
end
