# frozen_string_literal: true

class VendorsController < ApplicationController # :nodoc:
  before_action :set_vendor, only: %i[show destroy update]
  respond_to :json

  def index
    respond_with Vendor.all
  end

  def show
    render json: @vendor
  end

  def create
    return unauthorized unless admin?(current_user)

    respond_with Vendor.create(vendor_params)
  end

  def destroy
    return unauthorized unless admin?(current_user)

    @vendor.destroy
    head :no_content
  end

  def update
    return unauthorized unless admin?(current_user)

    @vendor.update(vendor_params)
    respond_with @vendor, json: @vendor
  end

  private

  def set_vendor
    @vendor = Vendor.find(params[:id])
  end

  def vendor_params
    params.require(:vendor).permit(:name, :address, :phone_no)
  end
end
