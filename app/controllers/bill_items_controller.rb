# frozen_string_literal: true

class BillItemsController < ApplicationController
  def index
    bill_items = if admin?(current_user)
                   BillItem.all
                 else
                   BillItem.joins(:shipment).where(shipments: { destination_id: current_user.site_id })
                 end
    render json: bill_items
  end

  def show
    bill_item = BillItem.find(params[:id])
    render json: bill_item
  end
end
