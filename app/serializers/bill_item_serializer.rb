# frozen_string_literal: true

class BillItemSerializer < ActiveModel::Serializer
  attributes :id, :bill_id, :shipment_id, :rate, :excise, :discount,
             :bill_no, :bill_date, :vehicle_no, :chalan_no, :vendor_name,
             :site_name, :quantity, :shipment_date, :unit, :brand,
             :description, :additional_units, :remarks, :product_name,
             :amount, :category_name

  def bill_no
    object.bill.bill_no
  end

  def bill_date
    object.bill.date
  end

  def vehicle_no
    object.bill.vehicle_no
  end

  def chalan_no
    object.bill.chalan_no
  end

  def category_name
    object.shipment.product.category.name
  end

  def product_name
    object.shipment.product.name
  end

  def vendor_name
    object.bill.vendor.name
  end

  def site_name
    object.bill.site.name
  end

  def quantity
    object.shipment.quantity
  end

  def shipment_date
    object.shipment.date
  end

  def unit
    object.shipment.unit
  end

  def brand
    object.shipment.brand
  end

  def description
    object.shipment.description
  end

  def additional_units
    object.shipment.additional_units
  end

  def remarks
    object.shipment.remarks
  end
end
