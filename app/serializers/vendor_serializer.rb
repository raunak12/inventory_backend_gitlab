# frozen_string_literal: true

class VendorSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :phone_no
end
