# frozen_string_literal: true

class RepatriationSerializer < ActiveModel::Serializer
  attributes :id, :date, :reason, :chalan_no, :repatriated_items, :source_id, :source_type, :destination_id, :destination_type

  def repatriated_items
    object.repatriated_items.map do |repatriated_item|
      shipment = repatriated_item.shipment
      {
        repatriation_item_id: repatriated_item.id,
        shipment_id: shipment.id,
        product_id: shipment.product_id,
        category_id: shipment.product.category.id,
        unit: shipment.unit,
        quantity: shipment.quantity
      }
    end
  end

  def source_id
    object.shipments.map(&:source_id).first
  end

  def source_type
    object.shipments.map(&:source_type).first
  end

  def destination_type
    object.shipments.map(&:destination_type).first
  end

  def destination_id
    object.shipments.map(&:destination_id).first
  end
end
