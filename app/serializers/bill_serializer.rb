# frozen_string_literal: true

class BillSerializer < ActiveModel::Serializer
  attributes :id, :bill_no, :date, :vehicle_no, :chalan_no, :vendor_id, :site_id, :bill_items

  def bill_items
    object.bill_items.map do |bill_item|
      shipment = bill_item.shipment
      {
        bill_item_id: bill_item.id,
        shipment_id: shipment.id,
        rate: bill_item.rate,
        excise: bill_item.excise,
        amount: bill_item.amount,
        date: shipment.date,
        product_id: shipment.product_id,
        category_id: shipment.product.category.id,
        description: shipment.description,
        brand: shipment.brand,
        unit: shipment.unit,
        quantity: shipment.quantity,
        discount: bill_item.discount,
        remarks: shipment.remarks,
        additional_informations: shipment.additional_units
      }
    end
  end
end
