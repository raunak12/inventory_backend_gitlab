# frozen_string_literal: true

class ShipmentSerializer < ActiveModel::Serializer
  attributes :id, :quantity, :date, :unit, :brand, :description,
             :additional_units, :remarks, :product_id
end
