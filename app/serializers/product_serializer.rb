# frozen_string_literal: true

class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :category_id, :unit, :category_name

  def category_id
    object.category.id
  end

  def category_name
    object.category.name
  end

  def unit
    object.category.unit
  end
end
