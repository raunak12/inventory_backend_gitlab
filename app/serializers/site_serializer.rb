# frozen_string_literal: true

class SiteSerializer < ActiveModel::Serializer
  attributes :id, :name, :contact_person, :phone_no
end
