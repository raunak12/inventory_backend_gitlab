# frozen_string_literal: true

class DispatchedItemSerializer < ActiveModel::Serializer
  attributes :id, :dispatch_id, :shipment_id, :shipment_date,
             :bill_no, :date, :vehicle_no, :chalan_no,
             :quantity, :unit, :brand, :description, :remarks,
             :product_name, :category, :source, :destination

  def chalan_no
    object.dispatch.chalan_no
  end

  def shipment_date
    object.shipment.date
  end

  def bill_no
    object.dispatch.bill_no
  end

  def dispatch_id
    object.dispatch.id
  end

  def date
    object.dispatch.date
  end

  def vehicle_no
    object.dispatch.vehicle_no
  end

  def quantity
    object.shipment.quantity
  end

  def unit
    object.shipment.unit
  end

  def brand
    object.shipment.brand
  end

  def description
    object.shipment.description
  end

  def remarks
    object.shipment.remarks
  end

  def product_name
    object.shipment.product.name
  end

  def category
    object.shipment.product.category.name
  end

  def source
    object.shipment.source.name
  end

  def destination
    object.shipment&.destination&.name
  end
end
