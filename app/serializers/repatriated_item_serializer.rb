# frozen_string_literal: true

class RepatriatedItemSerializer < ActiveModel::Serializer
  attributes :id, :shipment_id, :date, :chalan_no, :category_name, :shipment_date,
             :product_name, :source_name, :source_type, :destination_name, :remarks,
             :destination_type, :quantity, :reason, :unit, :repatriation_id

  def repatriation_id
    object.repatriation.id
  end

  def date
    object.repatriation.date
  end

  def shipment_date
    object.shipment.date
  end

  def chalan_no
    object.repatriation.chalan_no
  end

  def category_name
    object.shipment.product.category.name
  end

  def product_name
    object.shipment.product.name
  end

  def source_name
    object.shipment.source.name
  end

  def source_type
    object.shipment.source_type
  end

  def destination_name
    object.shipment.destination.name
  end

  def destination_type
    object.shipment.destination_type
  end

  def quantity
    object.shipment.quantity
  end

  def reason
    object.repatriation.reason
  end

  def unit
    object.shipment.unit
  end

  def remarks
    object.shipment.remarks
  end
end
