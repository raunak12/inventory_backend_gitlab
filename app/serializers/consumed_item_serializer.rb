# frozen_string_literal: true

class ConsumedItemSerializer < ActiveModel::Serializer
  attributes :id, :shipment_id, :consumption_id, :shipment_date,
             :date, :building_no, :product, :category, :remarks,
             :quantity, :unit, :source_type, :source_name, :destination_type, :destination_name

  def date
    object.consumption.date
  end

  def shipment_date
    object.shipment.date
  end

  def consumption_id
    object.consumption.id
  end

  def building_no
    object.consumption.building_no
  end

  def product
    object.shipment&.product&.name
  end

  def category
    object.shipment.product.category.name
  end

  def remarks
    object.shipment.remarks
  end

  def quantity
    object.shipment.quantity
  end

  def unit
    object.shipment.unit
  end

  def source_type
    object.shipment.source_type
  end

  def source_name
    object.shipment.source&.name
  end

  def destination_type
    object&.shipment&.destination_type
  end

  def destination_name
    object&.shipment&.destination&.name
  end
end
