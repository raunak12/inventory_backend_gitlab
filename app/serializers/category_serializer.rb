# frozen_string_literal: true

class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :unit, :quantity_limit, :additional_field_required, :additional_fields
end
