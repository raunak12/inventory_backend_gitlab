# frozen_string_literal: true

class DispatchSerializer < ActiveModel::Serializer
  attributes :id, :date, :bill_no, :vehicle_no, :chalan_no, :dispatched_items, :source_id, :destination_id

  def dispatched_items
    object.dispatched_items.map do |dispatched_item|
      shipment = dispatched_item.shipment
      {
        dispatched_item_id: dispatched_item.id,
        shipment_id: shipment.id,
        product_id: shipment.product.id,
        category_id: shipment.product.category.id,
        description: shipment.description,
        brand: shipment.brand,
        unit: shipment.unit,
        quantity: shipment.quantity,
        remarks: shipment.remarks,
        vehicle_no: dispatched_item.dispatch.vehicle_no,
        chalan_no: dispatched_item.dispatch.chalan_no
      }
    end
  end

  def source_id
    object.shipments.map(&:source_id).first
  end

  def destination_id
    object.shipments.map(&:destination_id).first
  end
end
