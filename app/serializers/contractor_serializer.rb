# frozen_string_literal: true

class ContractorSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :phone_no
end
