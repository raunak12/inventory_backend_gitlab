# frozen_string_literal: true

class ConsumptionSerializer < ActiveModel::Serializer
  attributes :id, :date, :building_no, :consumed_items, :source_id, :destination_id, :source_type, :destination_type

  def consumed_items
    object.consumed_items.map do |consumed_item|
      shipment = consumed_item.shipment
      {
        consumed_item_id: consumed_item.id,
        shipment_id: shipment.id,
        product_id: shipment&.product&.id,
        category_id: shipment.product.category.id,
        unit: shipment.unit,
        quantity: shipment.quantity,
        remarks: shipment.remarks
      }
    end
  end

  def source_type
    object.shipments.map(&:source_type).first
  end

  def destination_type
    object.shipments.map(&:destination_type).first
  end

  def source_id
    object.shipments.map(&:source_id).first
  end

  def destination_id
    object.shipments.map(&:destination_id).first
  end
end
