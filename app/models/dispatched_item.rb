# frozen_string_literal: true

class DispatchedItem < ApplicationRecord
  belongs_to :shipment
  belongs_to :dispatch
  accepts_nested_attributes_for :shipment
end
