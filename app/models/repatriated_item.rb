# frozen_string_literal: true

class RepatriatedItem < ApplicationRecord
  belongs_to :repatriation
  belongs_to :shipment
  accepts_nested_attributes_for :shipment
end
