# frozen_string_literal: true

class Shipment < ApplicationRecord
  belongs_to :source, polymorphic: true
  belongs_to :destination, polymorphic: true
  belongs_to :product

  has_one :bill_item, dependent: :destroy
  has_one :bill, through: :bill_item

  has_one :dispatched_item, dependent: :destroy
  has_one :dispatch, through: :dispatched_item

  has_one :consumed_item, dependent: :destroy
  has_one :consumption, through: :consumed_item

  has_one :repatriated_item, dependent: :destroy
  has_one :repatriation, through: :repatriated_item

  scope :join_sites, lambda {
    joins("INNER JOIN sites ON (shipments.source_type = 'Site' AND sites.id = shipments.source_id) OR (shipments.destination_type = 'Site' AND sites.id = shipments.destination_id)")
  }

  scope :site_product_details, lambda { |start_date, end_date|
    join_sites
      .left_outer_joins(:bill)
      .left_outer_joins(:dispatch)
      .left_outer_joins(:consumption)
      .left_outer_joins(:repatriation)
      .select(
        'shipments.*',
        'sites.name AS site_name',
        'sites.id AS site_id',
        "(CASE
          WHEN bills.id IS NOT NULL
            THEN shipments.quantity
          ELSE
            0
          END) AS purchase",
        "(CASE
        WHEN consumptions.id IS NOT NULL AND shipments.source_type = 'Site'
          THEN shipments.quantity
        ELSE
          0
        END) AS consumed",
        "(CASE
        WHEN dispatches.id IS NOT NULL AND shipments.destination_type = 'Site' AND shipments.destination_id = sites.id
          THEN shipments.quantity
        ELSE
          0
        END) AS dispatch_in",
        "(CASE
        WHEN dispatches.id IS NOT NULL AND shipments.source_type = 'Site' AND shipments.source_id = sites.id
          THEN shipments.quantity
        ELSE
          0
        END) AS dispatch_out",
        "(CASE
         WHEN repatriations.id IS NOT NULL AND shipments.destination_type = 'Site'
           THEN shipments.quantity
         ELSE
           0
         END) AS repatriated"
      )
      .filter_report_by_date(start_date, end_date)
  }

  scope :site_reports, lambda { |start_date, end_date|
    Shipment.from(site_product_details(start_date, end_date), :shipments)
            .select(
              'site_id',
              :product_id,
              'sum(purchase) total_purchase',
              'sum(consumed) total_consumed',
              'sum(dispatch_in) total_dispatch_in',
              'sum(dispatch_out) total_dispatch_out',
              'sum(repatriated) total_repatriation',
              '(sum(purchase) + sum(dispatch_in) - sum(consumed) - sum(dispatch_out) + sum(repatriated)) closing'
            )
            .group(:product_id, :site_id)
  }

  scope :join_vendors, lambda { |start_date, end_date|
    joins("INNER JOIN vendors ON (shipments.source_type = 'Vendor' AND vendors.id = shipments.source_id)")
      .left_outer_joins(:bill)
      .select('shipments.*', 'vendors.name AS vendor_name', 'vendors.id AS vendor_id')
      .filter_report_by_date(start_date, end_date)
  }

  scope :vendor_reports, lambda { |start_date, end_date|
    Shipment.from(join_vendors(start_date, end_date), :shipments)
            .select('vendor_id, product_id, sum(quantity) total_quantity').group('vendor_id, product_id')
  }

  scope :join_contractors, lambda {
    joins("INNER JOIN contractors ON (shipments.source_type = 'Contractor' AND contractors.id = shipments.source_id)
     OR (shipments.destination_type = 'Contractor' AND contractors.id = shipments.destination_id)")
  }

  scope :contractor_details, lambda { |start_date, end_date|
    join_contractors
      .left_outer_joins(:consumption)
      .left_outer_joins(:repatriation)
      .select(
        'shipments.*',
        'contractors.name AS contractor_name',
        'contractors.id AS contractor_id',
        "(CASE
        WHEN consumptions.id IS NOT NULL AND shipments.destination_type = 'Contractor'
          THEN shipments.quantity
        ELSE
          0
        END) AS consumption",
        "(CASE
        WHEN repatriations.id IS NOT NULL AND shipments.source_type = 'Contractor'
          THEN shipments.quantity
        ELSE
          0
        END) AS repatriation"
      )
      .filter_report_by_date(start_date, end_date)
  }

  scope :contractor_reports, lambda { |start_date, end_date|
    Shipment.from(contractor_details(start_date, end_date), :shipments)
            .select(
              'contractor_id',
              :product_id,
              'sum(consumption) total_consumption',
              'sum(repatriation) total_repatriation',
              '(sum(consumption) - sum(repatriation)) total_consumed'
            )
            .group(:contractor_id, :product_id)
  }

  scope :filter_report_by_site_id, lambda { |site_id|
    where(site_id: site_id) if site_id.present?
  }

  scope :filter_report_by_product_id, lambda { |product_id|
    where(product_id: product_id) if product_id.present?
  }

  def self.filter_report_by_date(start_date, end_date)
    if start_date.present? && end_date.present?
      where(date: start_date...end_date)
    elsif end_date.present?
      where('shipments.date < ?', end_date)
    else
      all
    end
  end

  scope :filter_report_by_vendor_id, lambda { |vendor_id|
    where(vendor_id: vendor_id) if vendor_id.present?
  }

  scope :filter_report_by_contractor_id, lambda { |contractor_id|
    where(contractor_id: contractor_id) if contractor_id.present?
  }

  def self.final_vendor_reports(options = {})
    options = options.with_indifferent_access
    Shipment.from(vendor_reports(options[:start_date], options[:end_date]),
                  :shipments)
            .joins('inner join vendors on vendors.id = shipments.vendor_id')
            .select(
              'shipments.*',
              'vendors.name vendor_name'
            )
            .filter_report_by_vendor_id(options[:vendor_id])
            .filter_report_by_product_id(options[:product_id])
  end

  def self.final_site_reports(options = {})
    options = options.with_indifferent_access
    Shipment.from(
      site_reports(options[:start_date], options[:end_date]),
      :shipments
    )
            .includes(:product)
            .joins('inner join sites on sites.id = shipments.site_id')
            .select(
              'shipments.*',
              'sites.name site_name'
            )
            .filter_report_by_site_id(options[:site_id])
            .filter_report_by_product_id(options[:product_id])
  end

  def self.final_contractor_reports(options = {})
    options = options.with_indifferent_access
    Shipment.from(
      contractor_reports(options[:start_date], options[:end_date]),
      :shipments
    )
            .joins('inner join contractors on contractors.id = shipments.contractor_id')
            .select(
              'shipments.*',
              'contractors.name contractor_name'
            )
            .filter_report_by_contractor_id(options[:contractor_id])
            .filter_report_by_product_id(options[:product_id])
  end
end
