# frozen_string_literal: true

class ConsumedItem < ApplicationRecord
  belongs_to :shipment
  belongs_to :consumption
  accepts_nested_attributes_for :shipment
end
