# frozen_string_literal: true

class Dispatch < ApplicationRecord
  has_many :dispatched_items
  has_many :shipments, through: :dispatched_items
  accepts_nested_attributes_for :dispatched_items
end
