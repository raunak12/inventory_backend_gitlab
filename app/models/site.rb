# frozen_string_literal: true

class Site < ApplicationRecord
  has_many :sent_shipments, class_name: 'Shipment', as: :source
  has_many :received_shipments, class_name: 'Shipment', as: :destination
  has_many :bills
end
