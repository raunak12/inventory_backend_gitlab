# frozen_string_literal: true

class Contractor < ApplicationRecord
  has_many :sent_shipments, class_name: 'Shipment', as: :source
  has_many :received_shipments, class_name: 'Shipment', as: :destination
end
