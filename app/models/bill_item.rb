# frozen_string_literal: true

class BillItem < ApplicationRecord
  belongs_to :bill
  belongs_to :shipment
  accepts_nested_attributes_for :shipment
end
