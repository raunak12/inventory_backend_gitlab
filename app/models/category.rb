# frozen_string_literal: true

class Category < ApplicationRecord
  has_many :products

  before_save :check_for_additional_fields

  def check_for_additional_fields
    self.additional_fields = [] unless additional_field_required?
  end
end
