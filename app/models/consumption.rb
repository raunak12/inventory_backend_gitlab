# frozen_string_literal: true

class Consumption < ApplicationRecord
  has_many :consumed_items
  has_many :shipments, through: :consumed_items
  accepts_nested_attributes_for :consumed_items
end
