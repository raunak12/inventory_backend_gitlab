# frozen_string_literal: true

class Bill < ApplicationRecord
  has_many :bill_items
  has_many :shipments, through: :bill_items
  validates :bill_no, uniqueness: { scope: :vendor_id }
  accepts_nested_attributes_for :bill_items
  belongs_to :vendor
  belongs_to :site
end
