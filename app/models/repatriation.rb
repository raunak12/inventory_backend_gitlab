# frozen_string_literal: true

class Repatriation < ApplicationRecord
  has_many :repatriated_items
  has_many :shipments, through: :repatriated_items
  accepts_nested_attributes_for :repatriated_items
end
